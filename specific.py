import asyncio
from nats.aio.client import Client as NATS


async def run(loop):
    nc = NATS()
    await nc.connect(io_loop=loop, servers=["nats://localhost:4223"])

    print('Connected')
    await nc.close()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop))
    loop.close()